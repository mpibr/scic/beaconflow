# BeaconFlow

## work with the project

- download the code
```
% git clone https://gitlab.mpcdf.mpg.de/mpibr/scic/beaconflow.git
% cd beaconflow
```

- add data to the project by adding path variables to the yaml file
- work with git
```
# check current changes
% git status

# add changes
% git add .

# commit changes
% git commit -m "my new changes"

# push changes to remote repository
% git push
```

## create micromamba environment

- [install](https://mamba.readthedocs.io/en/latest/installation/micromamba-installation.html) micromamba on macOS
```
# use homebrew
% brew install micromamba

# activate micromamba in your profile
% micromamba activate
(base) %
```

- create empty project environment
```
% micromamba create -p ./venv
Empty environment created at prefix: /Users/<user>/pathto/beaconflow/venv
% micromamba activate -p ./venv
(/Users/<user>/pathto/beaconflow/venv) <user>@M-00001 beaconflow % 
```

- install environment requirements from file
```
micromamba install -p ./venv -f environment.yaml
```

## use straighten tool
```
(venv) <user>@M-0001 beaconflow % python ./src/straighten.py ./data/raw/test.tif ./data/rois/test.roi
```