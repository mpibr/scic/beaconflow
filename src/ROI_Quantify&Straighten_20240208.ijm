//select dendrites, measure and staighten
//this script takes images (of dendrites)
//asks the user to draw an ROI on the dendrite
//it quantifies a set of measurements on the selected dendrite
//creates another image of the straighten dendrite


//get directory of the folder where the images are and of the folder where you want to put the straighten dendrites
////please note they have to be two different folders!
// Prompt for ROI width first
Dialog.create("ROI width");
Dialog.addNumber("How wide should the ROI be (in pixels)?", 50);
Dialog.show();
roi_width = Dialog.getNumber();

// Now, prompt for directories separately using getDirectory()
input_folder = getDirectory("Select Input Directory");
results_folder = getDirectory("Select Results and Analysis Files Directory");



close("Roi Manager");
//set what measurements you want to save
run("Set Measurements...", "area mean standard integrated median redirect=None decimal=3"); //you can change this according to what you need

//List of all filenames in the directory of interest
file_list = getFileList(input_folder);
file_n = file_list.length; //get number of files in the folder 
print(file_n);

//loop through files
	
for (i=0; i<file_n; i++) {
    path = input_folder + file_list[i];
    if (isTif(path)) {
        
        open(path);
        name = File.getName(path);
        name_short = File.getNameWithoutExtension(path);
        print(name);
        
        // Adjust auto brightness and contrast to better see the dendrite to trace
        run("Brightness/Contrast...");
        run("Enhance Contrast", "saturated=2.5");
        
        // Set up line tool and line width
        setTool("polyline");
        run("Line Width...", "line=" + roi_width);
        
        // Ask user to draw a line on the dendrite
        do {
            waitForUser("Draw on one dendrite.\nPlease always start from close to the soma.\nPress 't' to add each ROI to the manager, and press OK when you're done");
        } while (roiManager("count")==0);
        
        //Revert back to original LUT for analysis
	     resetMinAndMax();

        
        runAnalysis();
    }
}

waitForUser("Congrats!", "You're done with the tif files for this folder.");
	




// ---------------------------FUNCTIONS--------------------------------------------------
//Analysis function
function runAnalysis() {
	
	roi_n = roiManager("count");
	
	for(i=0; i<roi_n; i++) {
	selectWindow(name);
	roiManager("Select", i);
	roiManager("Rename", name + (i+1));
	roiManager("Save", results_folder + name_short + "_" + (i+1) + ".roi");
	
	roiManager("Multi Measure");
	saveAs("Results", results_folder + name_short + "_" + (i+1) + ".csv");
	
	run("Straighten...", "title=" + name_short + "_" + (i+1) + "_straight.tif line=" + roi_width + " process");
	saveAs("Tiff", results_folder + name_short + "_" + (i+1) + "_straight.tif");
	close();
	close("Results");
	}
	close("*");
	
	roiManager("Deselect");
	roiManager("delete");
}

//Function to test if a file is a tif image
function isTif(filename) {
	extensions = newArray("tif", "tiff");
	result = false;
	for (i=0; i<extensions.length; i++) {
		if (endsWith(toLowerCase(filename), "." + extensions[i]))
			result = true;
        }
	return result;
    }

// -----------------------------------------------------------------------------
