%% test_rRNA_segmentation
clc
clear variables
close all

%% read image to stack
file_image = './dendrites/20240209_NKA25_1_Series002_RAW_ch00_1.tif';
iinfo = imfinfo(file_image);
n_stack = size(iinfo, 1);
stack = zeros(iinfo(1).Height, iinfo(1).Width, n_stack);
for i = 1 : n_stack
    stack(:,:,i) = imread(file_image, i);
end

%% segment particles
stack_smooth = imgaussfilt3(stack, 0.5);
med_block = 50;
i = 10;
se = strel('disk', 6);
results = []; % To store the centroids
tic
%for i = 1:n_stack
    % median filter
    med_start = max(1, i - floor(med_block / 2));
    med_end = min(i + floor(med_block / 2), n_stack);
    frame_med = median(stack_smooth(:,:,med_start:med_end), 3);
    frame = stack_smooth(:, :, i) - frame_med;
    
    % normalization
    frame_min = min(frame(:));
    frame_max = max(frame(:));
    frame_norm = (frame - frame_min) / (frame_max - frame_min);
    frame_norm = imgaussfilt(frame_norm, 2.0);
    
    % circular local maxima
    frame_dilate = imdilate(frame_norm, se);
    [centers, radii] = imfindcircles(frame_dilate, [6,15]);

    bry = (frame_norm >= frame_dilate) & (frame_norm > 0.35);
    rg = regionprops(bry, 'Centroid');
    test = cat(1, rg.Centroid);


    mask = zeros(size(frame));
    for j = 1:size(centers, 1)
        [xx, yy] = ndgrid((1:size(frame,1)) - centers(j,2), (1:size(frame,2)) - centers(j,1));
        mask((xx.^2 + yy.^2) <= radii(j)^2) = j;
    end
    props = regionprops(mask, stack_smooth(:,:,i), 'MeanIntensity');
    avg_intensity = cat(1, props.MeanIntensity);

    % centroids
    results_next = [centers, radii, avg_intensity, repmat(i, length(radii), 1)];
    results = [results; results_next];
%end
toc

%% example plot
figure('color', 'w');
imshow(frame_norm, []);
hold on;
plot(centers(:,1), centers(:,2), 'r.');
hold off;
