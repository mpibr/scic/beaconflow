import sys
import os
import numpy
import tifffile
import roifile
from tqdm import tqdm
from scipy.interpolate import PchipInterpolator
from scipy.ndimage import map_coordinates



def measure_polyline(xpoints, ypoints):
    distances = numpy.sqrt(numpy.diff(xpoints)**2 + numpy.diff(ypoints)**2)
    cumulative_length = numpy.insert(numpy.cumsum(distances), 0, 0)
    straight_length = int(numpy.ceil(cumulative_length[-1]))
    return straight_length, cumulative_length



def straighten_polyline(image, xpoints, ypoints, width):
    straight_length, cumulative_length = measure_polyline(xpoints, ypoints)
    straightened_image = numpy.zeros((width, straight_length), dtype=image.dtype)
    linspace = numpy.linspace(0, cumulative_length[-1], num=straight_length)
    interp_x = PchipInterpolator(cumulative_length, xpoints)(linspace)
    interp_y = PchipInterpolator(cumulative_length, ypoints)(linspace)
    
    dx = numpy.diff(interp_x, append=interp_x[-1])
    dy = numpy.diff(interp_y, append=interp_y[-1])
    
    norm = numpy.sqrt(dx**2 + dy**2) + 1e-8  # Add epsilon to avoid division by zero
    valid_norm = norm > 0
    perp_dx = numpy.zeros_like(dx)
    perp_dy = numpy.zeros_like(dy)
    perp_dx[valid_norm] = -dy[valid_norm] / norm[valid_norm]
    perp_dy[valid_norm] = dx[valid_norm] / norm[valid_norm]
    
    j_indices = numpy.arange(-width // 2, width // 2, dtype=numpy.float32)
    
    sample_x = numpy.outer(perp_dx, j_indices) + interp_x[:, None]
    sample_y = numpy.outer(perp_dy, j_indices) + interp_y[:, None]
    
    # Ensure coordinates are within image bounds
    sample_x = numpy.clip(sample_x, 0, image.shape[1] - 1)
    sample_y = numpy.clip(sample_y, 0, image.shape[0] - 1)
    
    sampled_values = map_coordinates(image, [sample_y.ravel(), sample_x.ravel()], order=1)
    
    straightened_image = sampled_values.reshape(straight_length, width).T

    return straightened_image



if __name__ == "__main__":
    
    if len(sys.argv) < 3:
        print("Usage: script.py <image_file> <roi_file>")
        sys.exit(1)

    file_image = sys.argv[1]
    file_roi = sys.argv[2]

    if not os.path.exists(file_image):
        print(f"Error: The file '{file_image}' does not exist.")
        sys.exit(1)
    if not os.path.exists(file_roi):
        print(f"Error: The file '{file_roi}' does not exist.")
        sys.exit(1)

    file_out = file_roi.replace(".roi", ".tif")

    # load click positions
    roi = roifile.ImagejRoi.fromfile(file_roi)
    clicks = roi.coordinates()
    xpoints = clicks[:,0]
    ypoints = clicks[:,1]
    straighten_length, _ = measure_polyline(xpoints, ypoints)

    with tifffile.TiffFile(file_image) as th_in:
        total_pages = len(th_in.pages)
        metadata_dict = {tag.name: tag.value for tag in th_in.pages[0].tags.values()}
        stack = numpy.zeros((total_pages, roi.stroke_width, straighten_length), dtype= th_in.pages[0].dtype)
        print(f"Total pages in TIFF: {total_pages}")
        for i, page in enumerate(tqdm(th_in.pages, total=total_pages)):
            frame = page.asarray()
            stack[i] = straighten_polyline(frame, xpoints, ypoints, roi.stroke_width)
    print("Processing complete.")

    # write straightened image
    tifffile.imwrite(file_out, stack, metadata=metadata_dict)
