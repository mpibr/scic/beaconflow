%% test_rRNA_tracking
clc
clear variables
close all

%% load results
load dendrites/20240209_NKA25_1_Series002_RAW_ch00_1.mat;

figure('color','w');
plot(results(:,1), results(:,2),'k.');



%% plot histogram of particles radii
%{
figure('color', 'w');
histogram(results(:,3), 100);
title(sprintf('detected %d particles in %d frames', size(results,1), max(results(:,5))), 'fontsize', 14);
xlabel('particle radius [pixels]', 'fontsize',14);
ylabel('counts', 'fontsize', 14);
set(gca, 'box','off');
print(gcf, '-dpng', '-r300', 'figureX_ParticleRadius.png');
%}

%% plot histogram of particles intensity
%{
figure('color', 'w');
histogram(results(:,4), 100);
title(sprintf('detected %d particles in %d frames', size(results,1), max(results(:,5))), 'fontsize', 14);
xlabel('particle intensity [IU 12bit]', 'fontsize',14);
ylabel('counts', 'fontsize', 14);
set(gca, 'box','off');
print(gcf, '-dpng', '-r300', 'figureX_ParticleIntensity.png');
%}

%% tracking particles
%{
xy = results(:,1:2);
frames = results(:,5);
ids = (1 : size(xy,1))';
n_frames = max(frames);
f = 2;
cost_list = zeros(n_frames, 1);
a = [];
tic
for f = 2 : n_frames
    cost_matrix = pdist2(xy(frames==f-1,:), xy(frames==f,:));
    [assignments, cost] = munkres(cost_matrix);
    a = cat(1, a, assignments);
    cost_list(f) = cost;
end
toc
%}
